#include <iostream>

using namespace std;

void thanks()
{
    cout << "thank you!" << endl;
}

int main()
{
    cout << "Enter Number" << endl;
    int steve;
    cin >> steve;
    thanks();
    cout << "You did not enter the number " << steve + 1 << endl;
   return 0;
}