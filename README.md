#include <iostream>

using namespace std;

int return1()
{
//The function is complete when it receives an integer, which it is subsequently defined by
    return 1;
}

//This function returns an integer input from the user
int getinfofromuser()
{
    int x;
    cin >> x;
    return x;
}

//This function returns an output "thank you!"
void thanks()
{
    cout << "thank you!" << endl;
}


int main()
{
    cout << "Hello" << endl;
    cout << "What's your favourite number?" << endl;
    int x = getinfofromuser();
    thanks();
    cout << "Could you repeat that? " << endl;
    cout << x + return1() << " ?" << endl;
    int y = getinfofromuser();
    cout << y - return1() << " ?" << endl;
    return 1;
}
